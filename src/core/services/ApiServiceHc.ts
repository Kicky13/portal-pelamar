import axios from "axios";

export default axios.create({
  baseURL: process.env.VUE_APP_API_HC_URL,
  headers: {
    "Content-type": "application/json",
    "Accept": "application/json",
    // "Authorization": "Bearer "+localStorage.getItem("token")
  }
});
